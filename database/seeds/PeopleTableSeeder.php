<?php

use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert([
            'fname' => "suttita",
            'lname' => "jankham",
            'age' => 22,
        ]);
    }
}
