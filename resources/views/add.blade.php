@extends('layouts.master')
@section('title','Add')
@section('content')
    <form action="{{ url('people') }}" method="POST">
        <div class="form-group">
            <label>firstname</label>
            <input type="text" class="from-control" name="fname">
        </div>
        <div class="form-group">
            <label>lastname</label>
            <input type="text" class="from-control" name="lname">
        </div>
        <div class="form-group">
            <label>age</label>
            <input type="text" class="from-control" name="age">
        </div>
        <button type="submit" class="btn btn-success">save</button>
    </form>
@endsection